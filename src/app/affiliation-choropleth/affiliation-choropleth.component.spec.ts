import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AffiliationChoroplethComponent } from './affiliation-choropleth.component';

describe('AffiliationChoroplethComponent', () => {
  let component: AffiliationChoroplethComponent;
  let fixture: ComponentFixture<AffiliationChoroplethComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AffiliationChoroplethComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AffiliationChoroplethComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
