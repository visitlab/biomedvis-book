import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
import { EnrichedZoteroPaper } from '../models/EnrichedZoteroPaper';
import { VisDataService } from '../services/vis-data.service';
import { BaseGraphComponent } from '../base-graph/base-graph.component';
import * as graphConfig from '../shared/graph-config';
import { AffiliationWrapper } from '../models/AffiliationWrapper';
import { FilteredResults } from '../models/FilteredResults';
import { PaperFilter } from '../models/PaperFilter';
import { GraphsEnum } from '../models/GraphsEnum';
import { InteractionUtil } from '../shared/interaction-util';
import * as interactionUtil from '../shared/interaction-util';
import { FilterType } from '../models/FilterType';

@Component({
  selector: 'app-affiliation-choropleth',
  templateUrl: './affiliation-choropleth.component.html',
  styleUrls: ['./affiliation-choropleth.component.scss']
})
export class AffiliationChoroplethComponent extends BaseGraphComponent implements OnInit {

  private colorRange = [
    "#ffffff",
    "#f7fcfd",
    "#e5f5f9",
    "#ccece6",
    "#99d8c9",
    "#66c2a4",
    "#41ae76",
    "#238b45",
    "#006d2c",
    "#00441b"
  ]

  private clickedTarget = ""
  private currentData: any[] = []

  constructor(protected visdataService: VisDataService) {
    super(visdataService);
  }

  override ngOnInit(): void {
    super.ngOnInit();
  }

  override prepareData(filteredResults: FilteredResults): any[] {

    let papers = filteredResults.getActivePapers()
    // In this case we want to find the journals affiliation
    let affiliationWrappers: AffiliationWrapper[] = [];

    for (var paper of papers) {
      let affiliations = paper.affiliation.split(",");
      for (var affiliation of affiliations) {

        // Remove blank space
        affiliation = affiliation.replace(/ /g, '')
        affiliationWrappers.push(
          new AffiliationWrapper(
            paper,
            affiliation
          )
        )
      }
    }

    let dataMap = d3.rollup(affiliationWrappers, v => v.length, d => d.affiliation);
    return Array.from(dataMap).sort();
  }

  override drawGraph(data: any[]): void {
    this.drawData(data)
  }

  override drawData(data: any[]): void {
    // Remember current data
    this.currentData = data
    this.findToolTip();

    const projection = d3.geoMercator();
    const path: any = d3.geoPath().projection(projection);

    let max = this.findMax(data)
    let ranges = this.findLinearRanges(max)

    let colorScale = d3.scaleThreshold<number, string>()
      .domain(ranges)
      //.domain([1, 5, 10, 20, 30, 50, 100, 200, 500])
      .range(this.colorRange);

    let g = this.svg.append('g');
    g.attr('class', 'map')
      .attr("transform",
        `translate(${this.width_margin}, ${this.height_margin})`);

    d3.json("https://raw.githubusercontent.com/holtzy/D3-graph-gallery/master/DATA/world.geojson")
      .then((topology: any) => {

        // Add count to topology country features
        for (var f of topology.features) {
          let count = 0;

          let countryName = f["properties"]["name"].toUpperCase()
          let countTuple = data.find(e => e[0] == countryName)

          if (countTuple === undefined) {
            count = 0
          } else {
            count = countTuple[1]
          }

          // Set the count in the json
          f["properties"]["count"] = count
        }

        // Then draw actual choropleth
        g.selectAll('path')
          .data(topology.features)
          .enter()
          .append('path')
          .attr('d', path)
          .attr("stroke", d => {
            return "grey"
          })
          .attr("fill", (d) => {

            var selectedCountry = this.visDataService.getCurrentFilter().getAffiliation()

            if (d["properties"]["name"].toUpperCase() === selectedCountry) {
              return interactionUtil.CLICKED_COLOR
            } else {
              let count = d["properties"]["count"]
              return colorScale(count)
            }
          })
      });

    g.on("mouseover", () => this.showToolTip())
      .on("mousemove", (event) => this.updateToolTip(event, null))
      .on("mouseout", () => this.hideToolTip());

    g.on("click", (event) => {
      // Set current clicked target country
      let countryName = event.target.__data__.properties["name"].toUpperCase()
      var selectedCountry = this.visDataService.getCurrentFilter().getAffiliation()
      if (selectedCountry === countryName) {
        this.clickedTarget = ""

        this.clearGraph()
        this.drawData(this.currentData)
        this.visDataService.filter(new PaperFilter(
          -1,
          "",
          "",
          "",
          [],
          -1,
          -1,
          -1,
          [GraphsEnum.AFFILIATION_CHOLOPLETH]),FilterType.Affiliation)
      } else {
        this.clickedTarget = event.target.__data__.properties["name"].toUpperCase()
        this.clearGraph()
        this.drawData(this.currentData)

        this.visDataService.filter(new PaperFilter(
          -1,
          "",
          "",
          this.clickedTarget,
          [],
          -1,
          -1,
          -1,
          [GraphsEnum.AFFILIATION_CHOLOPLETH]), FilterType.Affiliation)
      }
    })
    this.addGraphControls();
    this.drawLegend(ranges)

  }

  override getGraphConfig() {
    return new graphConfig.GraphConfig(GraphsEnum.AFFILIATION_CHOLOPLETH, "figure#affiliation_choropleth", "", "", false, true);
  }

  override getTitle(): string {
    return this.getGraphConfig().graphsEnum;
  }

  override clearGraph() {
    d3.selectAll("#affiliation_choropleth" + " > svg > g > *").remove();

    if(this.legendSvg) {
      d3.selectAll("#affiliation_choropleth" + " > svg > circle").remove();
      d3.selectAll("#affiliation_choropleth" + " > svg > text").remove();
    }
  }

  setupChloropleth(topology: any) {
  }

  addGraphControls() {
    // Setup zoom & pan event transformation
    let zoom = d3.zoom()
      .on('zoom', (e) => {
        console.log(e);
        d3.select(this.getGraphConfig().internalName + ' svg g').attr('transform', e.transform);
      }) as any
    d3.select(this.getGraphConfig().internalName + ' svg').call(zoom);
  }

  findMax(data: any[]): number {
    let max = 0
    for (var d of data) {
      if (d[1] > max) {
        max = d[1]
      }
    }
    return max
  }

  findLinearRanges(max: number): number[] {
    let initialStepSize = 10
    let nextStepSize = 100
    let finalRange = 500
    let members: number[] = []

    var i = 0
    while (i < max && i < finalRange) {
      if (i == 0) {
        members.push(1)
      } else {
        members.push(i)
      }
      // After we reached 100 we decided to jump in increments of 100
      if (i < nextStepSize) {
        i = i + initialStepSize
      } else {
        i = i + nextStepSize
      }
    }
    return members
  }

  findFibonacciRanges(max: number, members: number[]): number[] {
    if (members.length == 0) {
      members[0] = 0
      members[1] = 1
      members[2] = 1
    }
    if (members[members.length - 1] >= max) {
      // Remove the second element
      members.splice(0, 2)
      return members
    } else {
      let lastIndex = members.length - 1
      let secondTolastIndex = members.length - 2

      members.push(members[lastIndex] + members[secondTolastIndex])
      return this.findFibonacciRanges(max, members)
    }
  }

  override updateToolTip(event, node) {
    console.log("Move tool tip")
    let data = event.target.__data__.properties
    this.toolTip.style("top", (event.pageY - 10) + "px").style("left", (event.pageX + 10) + "px").html(`Country: <b>${data["name"]}</b><br>Count: <b>${data["count"]}</b>`)
  }

  drawLegend(data: any) {
    if (this.legendSvg) {
      var stepX = 220
      var stepY = 20
      let columnSize = 6

      var iconX = this.width_margin
      var iconY = 20

      var textX = iconX + 20
      var textY = iconY

      data.forEach((grouping, index) => {
        if (index < data.length - 1) {
          this.legendSvg.append("circle")
            .attr("cx", iconX)
            .attr("cy", iconY)
            .attr("r", 6)
            .attr("stroke", "black")
            .style("fill", this.colorRange[index])

          this.legendSvg.append("text")
            .attr("x", textX)
            .attr("y", textY)
            .text(`${grouping} - ${data[index + 1]}`) // Accessing the text from the linechart grouping
            .style("font-size", "10px")
            .attr("alignment-baseline", "middle")

          if ((index + 1) % columnSize == 0) {
            iconX += stepX
            iconY = 20
          } else {
            iconY += stepY
          }

          // Always update text at the end
          textX = iconX + 20
          textY = iconY

        }
      })
    }
  }
}
