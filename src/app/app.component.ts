import { Component } from '@angular/core';
import { MatFormField, MatFormFieldModule } from '@angular/material/form-field';

import { Subject, EMPTY, Observable, of, filter } from 'rxjs';
import { MatInputModule } from '@angular/material/input';
import { VisDataService } from './services/vis-data.service';
import { PaperFilter } from './models/PaperFilter';
import { PaperDataHelperService } from './services/paper-data-helper.service';
import { FilterType } from './models/FilterType';
import { InteractionUtil } from './shared/interaction-util';
import { Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { GraphsEnum } from './models/GraphsEnum';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  title = 'vis-dashboard';
  static hiddenListSubject = new Subject<boolean>();
  static hiddenFilterSubject = new Subject<boolean>();

  private visDataService: VisDataService;
  private paperDataHelperService: PaperDataHelperService;

  protected paperFilters: string[] = [];

  protected startYearValue: string = "1969";
  protected endYearValue: string = "2024";


  protected hiddenResults = true;
  protected hiddenFilters = true;

  constructor(@Inject(DOCUMENT) private _document: Document, visDataService: VisDataService, paperDataHelperService: PaperDataHelperService) {
    this.visDataService = visDataService;
    this.paperDataHelperService = paperDataHelperService;

    AppComponent.hiddenListSubject.subscribe(data => {
      this.setHiddenResults()
    })

    this.visDataService.paperFilterSubject.subscribe(paperFilter => {
      this.setPaperFilters(paperFilter)
    })

  }


  tabChanged(tabChangeEvent) {
    console.log("change")
    console.log('index => ', tabChangeEvent.index);

    if (tabChangeEvent.index == 1) {
      for (let i = 0; i < this._document.getElementsByClassName("graph-element").length; i++)
        (<HTMLCanvasElement>this._document.getElementsByClassName("graph-element")[i]).style.display = 'grid';
    } else {
      for (let i = 0; i < this._document.getElementsByClassName("graph-element").length; i++)
        (<HTMLCanvasElement>this._document.getElementsByClassName("graph-element")[i]).style.display = 'none';
    }




  }

  filterByTopic(unresolvedTopic: string) {
    let topic = this.paperDataHelperService.findTopicForTerm(unresolvedTopic)
    this.visDataService.filter(new PaperFilter(topic, ""), FilterType.Topic)
  }

  filterBySubject(subject: string) {
    if (subject) {
      this.visDataService.filter(new PaperFilter(-1, subject), FilterType.Subject)
    } else {
      this.visDataService.filter(new PaperFilter(-1, ""), FilterType.Subject)
    }
  }

  filterByStartYear(startYear: string) {
    console.log("startYear: " + startYear)
    if (startYear.length >= 4) {
      try {
        this.visDataService.filter(new PaperFilter(-1, "", "", "", [], Number(startYear)), FilterType.Start)
      } catch (error) {
        console.error(error)
      }
    } else {
      this.visDataService.filter(new PaperFilter(-1, "", "", "", [], -1), FilterType.Start)
    }
  }

  filterByEndYear(endYear: string) {
    console.log("endYear: " + endYear)
    if (endYear.length >= 4) {
      try {
        this.visDataService.filter(new PaperFilter(-1, "", "", "", [], -1, Number(endYear)), FilterType.End)
      } catch (error) {
        console.error(error)
      }
    } else {
      this.visDataService.filter(new PaperFilter(-1, "", "", "", [], -1), FilterType.End)
    }
  }

  filterByAuthorCount(authorCount: string) {
    try {
      this.visDataService.filter(new PaperFilter(-1, "", "", "", [], -1, -1, Number(authorCount)), FilterType.Count)
    } catch (error) {
      // Wasn't able to parse number here
      console.log(error)
    }
  }

  remove(filter: string){
    var topic = PaperDataHelperService.topicMap.get(this.visDataService.getCurrentFilter().getTopic()+1)
    topic = typeof topic === "undefined" ? [""] : topic;

    if(topic[0] == filter){
      this.visDataService.filter(new PaperFilter(
        -1,
        "",
        "",
        "",
        [],
        -1,
        -1,
        -1,
        [GraphsEnum.TOPIC_HISTOGRAM]),FilterType.Topic)
    }

    var journal = this.visDataService.getCurrentFilter().getJournal();
    if(journal == filter){
      this.visDataService.filter(new PaperFilter(
        -1,
        "",
        "",
        "",
        [],
        -1,
        -1,
        -1,
        [GraphsEnum.JOURNAL_LINECHART]), FilterType.Journal)
    }

    var country = this.visDataService.getCurrentFilter().getAffiliation()
    if(country == filter){
    this.visDataService.filter(new PaperFilter(
      -1,
      "",
      "",
      "",
      [],
      -1,
      -1,
      -1,
      [GraphsEnum.AFFILIATION_CHOLOPLETH]),FilterType.Affiliation)
    }

    var authors = this.visDataService.getCurrentFilter().getAuthorNames()
    authors.forEach(author =>{
      if(filter == author){
        var selectedAuthorNames = this.visDataService.getCurrentFilter().getAuthorNames()
        var id = selectedAuthorNames.indexOf(filter)
        selectedAuthorNames.splice(id,1)
        this.visDataService.filter(new PaperFilter(
          -1,
          "",
          "",
          "",
          selectedAuthorNames,
          -1,
          -1,
          -1,
          [GraphsEnum.AUTHOR_NETWORK]), FilterType.Author)


      }

    })

   
  }

  setHiddenResults() {
    this.hiddenResults = !this.hiddenResults
  }

  setHiddenFilters() {
    this.hiddenFilters = !this.hiddenFilters
  }

  setPaperFilters(paperFilter: PaperFilter) {
    // Resetting filters
    this.paperFilters = [];

    // Setting topic
    if (paperFilter.getTopic() + 1 && paperFilter.getTopic() != -1) {
      var area = PaperDataHelperService.topicMap.get(paperFilter.getTopic() + 1) ?? ""
      this.paperFilters.push(area[0])
    }

    // Setting journal
    if (paperFilter.getJournal()) {
      this.paperFilters.push(paperFilter.getJournal())
    }

    // Setting affiliation
    if (paperFilter.getAffiliation()) {
      this.paperFilters.push(paperFilter.getAffiliation())
    }

    // Setting author names filter
    if (paperFilter.getAuthorNames()) {

      if (paperFilter.getAuthorNames().length > 3) {
        this.paperFilters.push(`${paperFilter.getAuthorNames().length} authors`)
      }
      paperFilter.getAuthorNames().forEach((authorName) => {
        this.paperFilters.push(authorName)
        console.log("Pushed author to filters: " + authorName)
      })
    }

    this.startYearValue = this.visDataService.getCurrentFilter().getStartYear() > 0 ? this.visDataService.getCurrentFilter().getStartYear().toString() : "1969";
    this.endYearValue = this.visDataService.getCurrentFilter().getEndYear() > 0 ? this.visDataService.getCurrentFilter().getEndYear().toString() : "2024";
  }

}