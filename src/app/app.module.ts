import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatChipsModule} from '@angular/material/chips';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatPaginatorModule } from '@angular/material/paginator';
import { JournalHistogramComponent } from './journal-histogram/journal-histogram.component';
import { JournalLinechartComponent } from './journal-linechart/journal-linechart.component';
import { GraphPickerComponent } from './graph-picker/graph-picker.component';
import { MatInputModule } from '@angular/material/input';
import { AuthorNetworkComponent } from './author-network/author-network.component';
import { AffiliationChoroplethComponent } from './affiliation-choropleth/affiliation-choropleth.component';
import { TopicHistogramComponent } from './topic-histogram/topic-histogram.component'
import { MatCardModule } from '@angular/material/card';
import { NgxCsvParserModule } from 'ngx-csv-parser';
import { HttpClientModule } from '@angular/common/http';
import { AuthorLinkHistogramComponent } from './author-link-histogram/author-link-histogram.component';
import { PaperListComponent } from './paper-list/paper-list.component';
import { MatPaginator } from '@angular/material/paginator';
import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
  declarations: [
    AppComponent,
    JournalHistogramComponent,
    JournalLinechartComponent,
    GraphPickerComponent,
    AuthorNetworkComponent,
    AffiliationChoroplethComponent,
    TopicHistogramComponent,
    AuthorLinkHistogramComponent,
    PaperListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    MatListModule,
    MatPaginatorModule,
    NgxCsvParserModule,
    HttpClientModule,
    MatChipsModule,
    MatTabsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
