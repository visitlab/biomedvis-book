/// <reference lib="webworker" />

import { AuthorDataWorker } from "./author-network/author-data.worker";
import { FilteredResults } from "./models/FilteredResults";

addEventListener('message', ({ data }) => {
  console.log("Author data worker received job message");
  let authorsAndLinks = AuthorDataWorker.getAuthorsAndLinks(data)
  postMessage(authorsAndLinks);
});
