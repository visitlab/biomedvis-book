import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthorLinkHistogramComponent } from './author-link-histogram.component';

describe('AuthorLinkHistogramComponent', () => {
  let component: AuthorLinkHistogramComponent;
  let fixture: ComponentFixture<AuthorLinkHistogramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AuthorLinkHistogramComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(AuthorLinkHistogramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
