import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
import { VisDataService } from '../services/vis-data.service';
import { BaseGraphComponent } from '../base-graph/base-graph.component';
import * as graphConfig from '../shared/graph-config';
import { Author, AuthorLink, AuthorsAndLinks } from '../models/AuthorsAndLinks';
import { WorkerService } from '../services/worker.service';
import { WorkerListener } from '../services/worker-listener';
import { FilteredResults } from '../models/FilteredResults';
import { GraphsEnum } from '../models/GraphsEnum';
import * as interactionUtil from '../shared/interaction-util';

@Component({
  selector: 'app-author-link-histogram',
  templateUrl: './author-link-histogram.component.html',
  styleUrls: ['./author-link-histogram.component.scss']
})
export class AuthorLinkHistogramComponent extends BaseGraphComponent implements OnInit, WorkerListener {

  public dataLoading = true;
  private authorsSet: Set<Author> = new Set<Author>();
  private authorLinks: AuthorLink[] = [];
  private rawData: any[] = [];
  
  private workerService;

  constructor(protected visdataService: VisDataService, workerService: WorkerService) {
    super(visdataService);
    this.workerService = workerService;
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.workerService.addWorkerListener(this)
  }

  override prepareData(filteredResults: FilteredResults): any[] {
    this.dataLoading = true
    return [];
  }

  override drawGraph(data: any[]): void {
    this.drawData(data)
  }

  onMessage(data: any) {
    this.dataLoading = false

    console.log("Received message in author histogram component.")
    let authorsAndLinks = data as AuthorsAndLinks
    this.authorsSet = authorsAndLinks.authorSet;
    this.authorLinks = authorsAndLinks.authorLinks;
    this.dataLoading = false;

    let authorsList = Array.from(this.authorsSet.values());
    
    // Store a reference of the initial rolled up data to draw graph axis correctly
    this.rawData = Array.from(d3.rollup(authorsList, v => v.length, d => d.outboundLinks)).sort();

    // Some debugging help, finding size of active authors
    console.log("All authors:  " + authorsList.length)
    console.log("Active authors: " + authorsList.filter((author) => author.isActive == true).length)

    // Roll up of active authors that are being used for stacking
    let dataMap = d3.rollup(authorsList, v => v.length, d => d.outboundLinks, d => d.isActive);
    let dataArray = Array.from(dataMap);
    let mappedForStacking = dataArray.map(function (value, _) {

      let mappedJournalData: { [key: string]: number } = {
        numberOfLinks: Number(value[0]),
        active: value[1].get(true)!,
        inactive: value[1].get(false)!
      }
      return mappedJournalData
    });

    let stackingOperation = d3.stack().keys([interactionUtil.KEY_ACTIVE, interactionUtil.KEY_INACTIVE]);
    let stackedMap = stackingOperation(mappedForStacking)

    //let sortedDataMap = Array.from(dataMap).sort();
    this.drawGraph(stackedMap);
  }

  override drawData(data: any[]): void {
    this.clearGraph();
    this.findToolTip();

    this.drawAxisLabels()
    // Create the x-axis scale
    const x = d3.scaleBand()
      .range([0, this.width])
      .domain(this.rawData.map(d => d[0]))
      .padding(0.2);

    // Draw the x-axis
    this.svg.append("g")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end");

    // Create the y-axis scale
    const y = d3.scaleLinear()
      .domain([0, Math.max.apply(Math, this.rawData.map(function (d) { return d[1] }))])
      .range([this.height, 0]);

    // Create a color scale
    var colorScale = d3.scaleOrdinal()
    .domain([interactionUtil.KEY_ACTIVE, interactionUtil.KEY_INACTIVE])
    .range([interactionUtil.ACTIVE_COLOR, interactionUtil.INACTIVE_COLOR]);

    // Draw the y-axis
    this.svg.append("g")
      .call(d3.axisLeft(y));

    // Create and fill bars
    this.svg.selectAll("bars")
      .data(data)
      .enter().append("g")
      .attr("fill", function(d) { return colorScale(d.key); })
      .selectAll("rect")
        .data(function(d) { return d; })
        .enter()
        .append("rect")
        // We have to transform the number to a string here again
        .attr("x", d => x(d.data.numberOfLinks))
        .attr("y", k => y(k[1]))
        .attr("width", x.bandwidth())
        .attr("height", function(d) { return y(d[0]) - y(d[1]); })
        .on("mouseover", () => this.showToolTip())
        .on("mousemove", (event, bar) => this.updateToolTip(event, bar))
        .on("mouseout", () => this.hideToolTip());
  }

  override getGraphConfig() {
    return new graphConfig.GraphConfig(GraphsEnum.AUTHOR_LINK_HISTOGRAM, "figure#author_link_histogram", "Links to other authors", "Authors", true, false)
  }

  override getTitle(): string {
    return this.getGraphConfig().graphsEnum
  }

  override clearGraph() {
    d3.selectAll("#author_link_histogram" + " > svg > g > *").remove();
  }


  override updateToolTip(event, bar) {
    console.log("Move tool tip")
    this.toolTip
    .style("top", (event.pageY - 10) + "px")
    .style("left", (event.pageX + 10) + "px")
    .html(`Number of links: <b>${bar.data.numberOfLinks}</b>
    <br>
    Active papers: <b>${bar.data.active ?? 0}</b>
    <br>
    Inactive papers: <b>${bar.data.inactive ?? 0}</b>`)
  }

}
