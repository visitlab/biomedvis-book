import { EnrichedZoteroPaper } from "../models/EnrichedZoteroPaper";
import { Author, AuthorLink, AuthorsAndLinks } from "../models/AuthorsAndLinks";
import { FilteredResults } from "../models/FilteredResults";

export class AuthorDataWorker {

    static getAuthorsAndLinks(filteredResults: FilteredResults): AuthorsAndLinks {
        // Handling all papers here, because we want to be able to filter the full range of active authors
        // Unsure why but for some reason we need to create a new object here instead of
        // using the typed method parameter... 
        let results: FilteredResults = new FilteredResults([], [], -1)
        Object.assign(results, filteredResults)

        let papers = results.getAllPapers();
        let authorsSet = new Set<Author>();
        let authorLinks: AuthorLink[] = [];

        // First we find the authors
        var authorCounter = 0;
        papers.forEach((paper, index) => {
            let authors = this.splitAuthors(paper.author)

            for (var untrimmedAuthor of authors) {
                let author = untrimmedAuthor.trim();
                if (authorsSet.size == 0) {

                    // We give authors IDs here by using the current counter
                    authorsSet.add(new Author(authorCounter, author, 0, false))
                    authorCounter++;
                } else {
                    let containsAuthor = false;

                    for (var a of authorsSet.entries()) {
                        // Access the author through the value field of entries
                        // console.log("Auth1: " + a[1]["name"] + " , Auth2:" + author +"");
                        if (a[1]["name"] === author) {
                            containsAuthor = true;
                        }
                    }

                    if (!containsAuthor) {
                        authorsSet.add(new Author(authorCounter, author, 0, false))
                        authorCounter++;
                    }
                }
            }
        });

        // Then we find the links between authors
        papers.forEach((paper, index) => {
            let authors = this.splitAuthors(paper.author)

            let authorIndex = 0;
            while (authorIndex < authors.length) {
                let currentAuthor = authors[authorIndex].trim();
                let currentIndex = authorIndex + 1;

                while (currentIndex < authors.length) {
                    let linkedAuthor = authors[currentIndex].trim()

                    var firstAuthorID = -1;
                    var secondAuthorID = -1;

                    authorsSet.forEach(a => {
                        if (a["name"] === currentAuthor) {
                            firstAuthorID = a["id"];
                            a.outboundLinks++;

                            // Setting the state of the author that was involved 
                            // in this paper to active, if the paper was active of course
                            if(paper.isActive) {
                                a.isActive = true;  
                            }  
                        }

                        if (a["name"] === linkedAuthor) {
                            secondAuthorID = a["id"];
                        }
                    });

                    if (firstAuthorID !== -1 && secondAuthorID !== -1) {
                        let areAuthorsActive = this.areAuthorsActive(authorsSet, firstAuthorID, secondAuthorID)
                        authorLinks.push(new AuthorLink(firstAuthorID, secondAuthorID, paper.title, areAuthorsActive));
                    }
                    currentIndex++;
                }
                authorIndex++;
            }
        });
        return AuthorDataWorker.filterAuthorsAndLinks(new AuthorsAndLinks(authorsSet, authorLinks), results.getAuthorCount());
    }

    static filterAuthorsAndLinks(authorsAndLinks: AuthorsAndLinks, authorCount: number ): AuthorsAndLinks {
        let authorLinks: AuthorLink[] = authorsAndLinks.authorLinks;

        // Sort authors
        let authorArray = Array.from(authorsAndLinks.authorSet).sort((a, b) => (a.outboundLinks > b.outboundLinks) ? 1 : -1);
        let beginIndex = authorArray.length - Math.ceil(authorArray.length / 30);
        let endIndex = authorArray.length - 1;
        // Take the upper 10 percent and filter authors and links by it
        var authorSubArray = authorArray.slice(beginIndex, endIndex)
        
        // Now also filter by author active state and author count
        let activeAuthorSubArray = authorSubArray.filter(function (author) {
            return author.isActive && (author.outboundLinks >= authorCount)
        })

        console.log(`activeAuthorSubArray size: ${activeAuthorSubArray.length}`)
        // Now filter links by comparing IDs
        let filteredAuthorLinks = authorLinks.filter(function (authorLink) {
            if (activeAuthorSubArray.find(a => a.id === authorLink.firstAuthor) &&
                activeAuthorSubArray.find(a => a.id === authorLink.secondAuthor) &&
                activeAuthorSubArray.find(a => a.outboundLinks >= authorCount)) {
                return true;
            } else {
                return false;
            }
        });
        console.log(`Active authors sub array size: ${filteredAuthorLinks.length}`)
        return new AuthorsAndLinks(new Set(activeAuthorSubArray), filteredAuthorLinks);
    }

    static splitAuthors(authors: string): string[] {
        return authors.split(";");
    }

    static areAuthorsActive(authorsSet: Set<Author>, firstAuthorID: number, secondAuthorID: number): boolean {
        let authors = authorsSet.values
        return true
    }
}