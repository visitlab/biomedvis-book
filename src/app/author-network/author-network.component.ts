import { VisDataService } from '../services/vis-data.service';
import { BaseGraphComponent } from '../base-graph/base-graph.component';
import { Component, OnInit } from '@angular/core';
import { EnrichedZoteroPaper } from '../models/EnrichedZoteroPaper';
import * as graphConfig from '../shared/graph-config';
import * as d3 from 'd3';
import { Author, AuthorLink, AuthorsAndLinks } from '../models/AuthorsAndLinks';
import { WorkerService } from '../services/worker.service';
import { WorkerListener } from '../services/worker-listener';
import { FilteredResults } from '../models/FilteredResults';
import { GraphsEnum } from '../models/GraphsEnum';
import * as interactionUtil from '../shared/interaction-util';
import { PaperFilter } from '../models/PaperFilter';
import { FilterType } from '../models/FilterType';

@Component({
  selector: 'app-author-network',
  templateUrl: './author-network.component.html',
  styleUrls: ['./author-network.component.scss']
})
export class AuthorNetworkComponent extends BaseGraphComponent implements OnInit, WorkerListener {

  public dataLoading = true;

  private authorsSet: Set<Author> = new Set<Author>();
  private authorLinks: AuthorLink[] = [];
  private workerService;
  private selectedIds: string[] = [];
  private selectedAuthorNames: string[] = [];

  constructor(protected visdataService: VisDataService, workerService: WorkerService) {
    super(visdataService);
    this.workerService = workerService;
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.workerService.addWorkerListener(this)
  }

  override prepareData(filteredResults: FilteredResults): any[] {
    this.dataLoading = true
    this.workerService.postMessage(filteredResults);
    return [];
  }

  override drawGraph(data: any[]): void {
    this.drawData(data)
  }

  onMessage(data: any) {
    this.dataLoading = false;
    console.log("Received message in author network component.")
    let authorsAndLinks = data as AuthorsAndLinks

    // Find the active authors and their authors links only
    this.findActiveAuthorsAndLinks(authorsAndLinks)
    this.drawGraph([]);
  }


  override drawData(data: any[]): void {
    this.addGraphControls();
    this.findToolTip();

    console.log(`Drawing authors set with size: ${this.authorsSet.size}`)
    let authorsArray = Array.from(this.authorsSet.values());
    const links = this.svg
      .selectAll("line")
      .data(this.authorLinks)
      .join("line")
      .style("stroke", "#aaa");

    const nodes = this.svg
      .selectAll("circle")
      .data(this.authorsSet)
      .join("circle")
      .attr("r", function (d) { return 3 * (Math.sqrt(d.outboundLinks / Math.PI)) }) // Size of node based on links
      .style("fill", interactionUtil.ACTIVE_COLOR)
      .style("stroke", "black")
      .on("mouseover", () => this.showToolTip())
      .on("mousemove", (event, node) => this.updateToolTip(event, node))
      .on("mouseout", () => this.hideToolTip())
      .on("click", (event, node) => {
        if (this.selectedIds.includes(node.id)) {
          this.selectedIds = this.selectedIds.filter((id, index) => id !== node.id)
          this.selectedAuthorNames = this.selectedAuthorNames.filter((id, index) => id !== node.name)

          this.svg.select("#author" + node.id).style("fill", interactionUtil.ACTIVE_COLOR)
        } else {
          this.selectedIds.push(node.id)
          this.selectedAuthorNames.push(node.name)

          this.svg.select("#author" + node.id).style("fill", interactionUtil.CLICKED_COLOR)

          this.visDataService.filter(new PaperFilter(
            -1,
            "",
            "",
            "",
            this.selectedAuthorNames,
            -1,
            -1,
            -1,
            [GraphsEnum.AUTHOR_NETWORK]), FilterType.Author)
        }
      });

    var simulation = d3.forceSimulation(authorsArray)
      .force("link", d3.forceLink().id(function (d) { return d["id"]; })
        .links(this.authorLinks))
      .force("charge", d3.forceManyBody().distanceMin(400))
      .force("center", d3.forceCenter(this.width / 2, this.height / 2))
      .on("end", ticked);

    function ticked() {
      links
        .attr("x1", function (d) { return d.source.x; })
        .attr("y1", function (d) { return d.source.y; })
        .attr("x2", function (d) { return d.target.x; })
        .attr("y2", function (d) { return d.target.y; });
      nodes
        .attr("cx", function (d) { return d.x; })
        .attr("cy", function (d) { return d.y; })
        .attr("id", function (d) { return "author" + d.id });
    }
  }

  override getGraphConfig(): graphConfig.GraphConfig {
    return new graphConfig.GraphConfig(GraphsEnum.AUTHOR_NETWORK, "figure#author_network", "", "", false, false);
  }

  override getTitle(): string {
    return this.getGraphConfig().graphsEnum;
  }

  override clearGraph() {
    d3.selectAll("#author_network" + " > svg > g > *").remove();
  }

  getOutboundLinksCounter(): number[] {
    var result: number[] = [];
    this.authorsSet.forEach((a) => {
      result.push(a.outboundLinks);
    });
    return result;
  }

  addGraphControls() {
    // Setup zoom & pan event transformation
    this.enableZoomAndPan()
  }

  onClickedZoomAndPan() {
    this.disableBrushing()
    this.enableZoomAndPan()
  }

  onClickedBrushing() {
    this.disableZoomAndPan()
    this.enableBrushing()
  }

  enableZoomAndPan() {
    let zoom = d3.zoom()
      .on('zoom', (e) => {
        console.log(e);
        d3.select(this.getGraphConfig().internalName + ' svg g').attr('transform', e.transform)
      }) as any
    d3.select(this.getGraphConfig().internalName + ' svg').call(zoom)
  }

  disableZoomAndPan() {
    let zoom = d3.zoom()
      .on('zoom', null) as any
    d3.select(this.getGraphConfig().internalName + ' svg').call(zoom)
  }

  enableBrushing() {
    this.svg.call(d3.brush().extent([[0, 0], [640, 480]])
      .on('start', (selection) => {
        console.log("Started brush")

        // Reset coloring and authors
        const points = this.svg.selectAll("circle")
        points.style('fill', interactionUtil.ACTIVE_COLOR)
        this.selectedAuthorNames = []
        this.visDataService.filter(new PaperFilter(
          -1,
          "",
          "",
          "",
          this.selectedAuthorNames,
          -1,
          -1,
          -1,
          [GraphsEnum.AUTHOR_NETWORK]), FilterType.Author)
      })
      .on('brush', (b) => {
        console.log("Brush")

        // Resetting current selected authors to not stack authors with each brush callback
        this.selectedAuthorNames = []
        let x0 = b.selection[0][0]
        let x1 = b.selection[1][0]

        let y0 = b.selection[0][1]
        let y1 = b.selection[1][1]

        const points = this.svg.selectAll("circle")
        points.style('fill', (d) => {
          let inRange = false

          if((d.x > x0 && d.x < x1) && (d.y > y0 && d.y < y1) ) {
            inRange = true
            this.selectedAuthorNames.push(d.name)
          }
          
          if (inRange) {
            return interactionUtil.CLICKED_COLOR
          } else {
            return interactionUtil.ACTIVE_COLOR
          }
        });

        // Finally, filter during the brushing
        this.visDataService.filter(new PaperFilter(
          -1,
          "",
          "",
          "",
          this.selectedAuthorNames,
          -1,
          -1,
          -1,
          [GraphsEnum.AUTHOR_NETWORK]), FilterType.Author)
      }));
  }

  disableBrushing() {
    d3.selectAll(".brush").remove()
  }

  override updateToolTip(event, node) {
    console.log("Move tool tip")
    this.toolTip.style("top", (event.pageY - 10) + "px").style("left", (event.pageX + 10) + "px").html(`<b>${node.name}</b><br>Links: ${node.outboundLinks}`)
  }

  findActiveAuthorsAndLinks(authorsAndLinks: AuthorsAndLinks) {
    this.authorsSet = new Set(Array.from(authorsAndLinks.authorSet).filter((author) => author.isActive));
    this.authorLinks = authorsAndLinks.authorLinks;

  }

  findAllAuthorsAndLinks(authorsAndLinks: AuthorsAndLinks) {
    this.authorsSet = authorsAndLinks.authorSet;
    this.authorLinks = authorsAndLinks.authorLinks;
  }
}
