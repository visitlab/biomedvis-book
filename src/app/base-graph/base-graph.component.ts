import { Component, OnInit } from '@angular/core';
import { EnrichedZoteroPaper } from '../models/EnrichedZoteroPaper';
import { FilteredResults } from '../models/FilteredResults';
import { VisDataService } from '../services/vis-data.service';
import * as d3 from 'd3';
import * as graphConfig from '../shared/graph-config';

@Component({
  selector: 'app-base-graph',
  templateUrl: './base-graph.component.html',
  styleUrls: ['./base-graph.component.scss']
})
export abstract class BaseGraphComponent implements OnInit {

  protected svg: any;
  protected legendSvg: any;

  protected data: any[];
  protected transformedData: any[];
  protected toolTip: any;

  protected visDataService: VisDataService;

  protected height_margin: number = 0;
  protected width_margin: number = 0;
  protected width: number = 0;
  protected height: number = 0;

  protected intialDraw: boolean = true;

  readonly ADDITIONAL_HEIGHT = 240;

  constructor(visDataService: VisDataService) {
    this.data = [];
    this.transformedData = [];
    this.visDataService = visDataService;

    this.height_margin = graphConfig.CHART_HEIGHT_MARGIN;
    this.width_margin = graphConfig.CHART_WIDTH_MARGIN;
    this.width = graphConfig.CHART_WIDTH - (2 * this.width_margin);
    this.height = graphConfig.CHART_HEIGHT - (2 * this.height_margin);
  }

  ngOnInit(): void {
    console.log("changed");
    this.visDataService.dataSubject.subscribe(filteredResults => {

      let isGraphLocked = filteredResults.getLockedGraphs().includes(this.getGraphConfig().graphsEnum)
      console.log("Is graph locked? Graph: " + this.getGraphConfig().graphsEnum + ", locked: " + isGraphLocked)
      if (this.intialDraw) {
        this.prepareGraph();
        this.drawAxisLabels();
        this.transformedData = this.prepareData(filteredResults);
        this.drawGraph(this.transformedData);
        this.intialDraw = false;

        //hide after intit
        let graphConfig: graphConfig.GraphConfig = this.getGraphConfig();
        let tag = '#'+graphConfig.internalName.substring(7)+'_container';
        let el = (<HTMLCanvasElement>d3.select(tag).node());
        el!.style.display = 'none'; 
      } else if (!isGraphLocked) {
        this.transformedData = this.prepareData(filteredResults);
        this.clearGraph();
        this.drawAxisLabels();
        this.drawData(this.transformedData);
      }
    });
  }

  prepareGraph(): void {
    let graphConfig: graphConfig.GraphConfig = this.getGraphConfig()

    this.width = ((<HTMLCanvasElement>d3.select('#graph-conatiner').node()).clientWidth/2)- (4 * this.width_margin)
    


    let height = this.height + (2 * this.height_margin)

    this.svg = d3.select(graphConfig.internalName)
      .append("svg")
      .attr("width", this.width + (2 * this.width_margin))
      .attr("height", height)
      .append("g")
      .attr("transform", "translate(" + this.width_margin + "," + this.height_margin + ")");

    if (this.getGraphConfig().requestSpaceForLegend) {
      this.legendSvg = d3.select(graphConfig.internalName)
        .append("svg")
        .attr("width", this.width + (2 * this.width_margin))
        .attr("height", this.ADDITIONAL_HEIGHT)
       
    }
  }

  drawAxisLabels(): void {
    let graphConfig: graphConfig.GraphConfig = this.getGraphConfig()

    // Check if axis handling is actually needed
    if (graphConfig.areAxisLabelsNeeded) {
      // Appending x axis label
      this.svg.append("text")
        .attr("transform",
          "translate(" + (this.width / 2 - this.width_margin / 2) + " ," +
          (this.height + this.height_margin - 4) + ")")
        .style("text-anchor", "middle")
        .text(graphConfig.xAxisLabel);

      // Appending y axis label
      this.svg.append("text")
        .attr("transform", "rotate(-90)")
        .attr("y", 0 - (this.height_margin + 4))
        .attr("x", 0 - (this.height / 2))
        .attr("dy", "1em")
        .style("text-anchor", "middle")
        .text(graphConfig.yAxisLabel);
    }
  }

  findToolTip() {
    if (!this.toolTip) {
      this.toolTip = d3.select(".tool-tip")
    }
  }

  showToolTip() {
    console.log("Show tool tip")
    this.toolTip.style("visibility", "visible")
  }

  hideToolTip() {
    console.log("Hide tool tip")
    this.toolTip.style("visibility", "hidden")
  }

  abstract prepareData(filteredResults: FilteredResults): any[]

  abstract drawGraph(data: any[]): void

  abstract drawData(data: any[]): void

  abstract getTitle(): string

  abstract getGraphConfig(): graphConfig.GraphConfig

  abstract clearGraph()

  abstract updateToolTip(event: any, data: any)
}

