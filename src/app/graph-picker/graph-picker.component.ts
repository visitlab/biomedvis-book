import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-graph-picker',
  templateUrl: './graph-picker.component.html',
  styleUrls: ['./graph-picker.component.scss']
})
export class GraphPickerComponent implements OnInit {

  graphItems = [
    { id: 0, name: 'journal-histgram', label: 'Journal Histogram' },
    { id: 1, name: 'topic-histogram', label: 'Topic Histogram' },
    { id: 2, name: 'author-link-histogram', label: 'Author Link Histogram' },
    { id: 3, name: 'journal-line', label: 'Journal Line Chart' },
    { id: 4, name: 'author-network', label: 'Author Networks' },
    { id: 5, name: 'country-choropleth', label: 'Country Choropleth' }
  ];

  constructor() { }

  ngOnInit(): void { }

}
