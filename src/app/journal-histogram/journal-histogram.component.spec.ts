import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JournalHistogramComponent } from './journal-histogram.component';

describe('JournalHistogramComponent', () => {
  let component: JournalHistogramComponent;
  let fixture: ComponentFixture<JournalHistogramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [JournalHistogramComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JournalHistogramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
