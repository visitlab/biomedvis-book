import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
import { EnrichedZoteroPaper } from '../models/EnrichedZoteroPaper';
import { VisDataService } from '../services/vis-data.service';
import { BaseGraphComponent } from '../base-graph/base-graph.component';
import * as graphConfig from '../shared/graph-config';
import { FilteredResults } from '../models/FilteredResults';
import { GraphsEnum } from '../models/GraphsEnum';
import * as interactionUtil from '../shared/interaction-util';
import { PaperFilter } from '../models/PaperFilter';
import { FilterType } from '../models/FilterType';


@Component({
  selector: 'app-journal-histogram',
  templateUrl: './journal-histogram.component.html',
  styleUrls: ['./journal-histogram.component.scss']
})
export class JournalHistogramComponent extends BaseGraphComponent implements OnInit {

  private rawData: any[] = [];
  
  constructor(protected visdataService: VisDataService) {
    super(visdataService);
  }

  override ngOnInit(): void {
    super.ngOnInit();
  }

  override prepareData(filteredResults: FilteredResults): any[] {
    // Keep the raw rolled up data for graph axis drawing
    this.rawData = Array.from(d3.rollup(filteredResults.getAllPapers(), v => v.length, d => d.publicationYear)).sort();

    // In this case we want to find the journals by year
    let dataMap = d3.rollup(filteredResults.getAllPapers(), v => v.length, d => d.publicationYear, d => d.isActive);
    let dataArray = Array.from(dataMap);
    let mappedForStacking = dataArray.map(function (value, _) {

      // At this point values are stored in an InternMap with the key values of "true" and "false"
      // which reflect the state of the activeness of the filtered paper.
      // value[1] at that point is the isActive flag, as outcome of the rollup by publicationYear and isActive 
      let mappedJournalData: { [key: string]: number } = {

        // We use the number field here for storing the year of the journal
        year: Number(value[0]),
        active: value[1].get(true)!,
        inactive: value[1].get(false)!
      }
      return mappedJournalData
    });

    let stackingOperation = d3.stack().keys([interactionUtil.KEY_ACTIVE, interactionUtil.KEY_INACTIVE]);
    let stackedMap = stackingOperation(mappedForStacking)
    return stackedMap
  }

  override drawGraph(data: any[]): void {
    this.drawData(data)
  }

  override drawData(data: any[]): void {
    this.findToolTip();
    this.drawAxisLabels()

    // Create the x-axis scale
    const x = d3.scaleBand()
      .range([0, this.width])
      .domain(this.rawData.map(d => d[0]))
      .padding(0.2);

    // Draw the x-axis
    this.svg.append("g")
      .attr("transform", "translate(0," + this.height + ")")
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end");

    // Create the y-axis scale
    const y = d3.scaleLinear()
      .domain([0, Math.max.apply(Math, this.rawData.map(function (d) { return d[1] }))])
      .range([this.height, 0]);

    // Draw the y-axis
    this.svg.append("g")
      .call(d3.axisLeft(y));

    // Create a color scale
    var colorScale = d3.scaleOrdinal()
      .domain([interactionUtil.KEY_ACTIVE, interactionUtil.KEY_INACTIVE])
      .range([interactionUtil.ACTIVE_COLOR, interactionUtil.INACTIVE_COLOR]);

    // Create and fill bars
    this.svg.selectAll("bars")
      .data(data)
      .enter().append("g")
      .attr("fill", function(d) { return colorScale(d.key); })
      .selectAll("rect")
        .data(function(d) { return d; })
        .enter()
        .append("rect")
        // We have to transform the number to a string here again
        .attr("x", d => x(d.data.year.toString()))
        .attr("y", k => y(k[1]))
        .attr("width", x.bandwidth())
        .attr("height", function(d) { return y(d[0]) - y(d[1]); })
        .on("mouseover", () => this.showToolTip())
        .on("mousemove", (event, bar) => this.updateToolTip(event, bar))
        .on("mouseout", () => this.hideToolTip())
        .on("click", (bar, d) => {
          var activeYears = [""]
          data[0].forEach(d =>{
            if(d.data.active != undefined){
            activeYears.push(d.data.year.toString())
          }

          })
          activeYears.splice(0,1)


          if(activeYears.length == 1 && activeYears[0] == d.data.year.toString()) {
            this.visDataService.filter(new PaperFilter(-1, "", "", "", [], -1), FilterType.Start)
            this.visDataService.filter(new PaperFilter(-1, "", "", "", [], -1, -1), FilterType.End)
          } else {
            this.visDataService.filter(new PaperFilter(-1, "", "", "", [], Number(d.data.year)), FilterType.Start)
            this.visDataService.filter(new PaperFilter(-1, "", "", "", [], -1, Number(d.data.year)), FilterType.End)
          }
        });
  }

  override getGraphConfig() {
     return new graphConfig.GraphConfig(GraphsEnum.JOURNAL_HISTOGRAM, "figure#journal_histogram", "Year", "Paper Count", true, false)
  }

  override getTitle(): string {
    return this.getGraphConfig().graphsEnum
  }

  override clearGraph() {
    d3.selectAll("#journal_histogram" + " > svg > g > *").remove();
  }

  override updateToolTip(event, bar) {
    console.log("Move tool tip")
    this.toolTip
      .style("top", (event.pageY - 10) + "px")
      .style("left", (event.pageX + 10) + "px")
      .html(`Year: <b>${bar.data.year}</b>
      <br>
      Active papers: <b>${bar.data.active ?? 0}</b>
      <br>
      Inactive papers: <b>${bar.data.inactive ?? 0}</b>`)
  }
}


