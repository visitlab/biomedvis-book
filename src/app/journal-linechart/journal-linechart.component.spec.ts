import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JournalLinechartComponent } from './journal-linechart.component';

describe('JournalLinechartComponent', () => {
  let component: JournalLinechartComponent;
  let fixture: ComponentFixture<JournalLinechartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [JournalLinechartComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JournalLinechartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
