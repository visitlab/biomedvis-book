import { VisDataService } from '../services/vis-data.service';
import { BaseGraphComponent } from '../base-graph/base-graph.component';
import { EnrichedZoteroPaper } from '../models/EnrichedZoteroPaper';
import * as graphConfig from '../shared/graph-config';
import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
import { PaperFilter } from '../models/PaperFilter';
import { FilteredResults } from '../models/FilteredResults';
import { GraphsEnum } from '../models/GraphsEnum';
import { PaperDataHelperService } from '../services/paper-data-helper.service';
import { InteractionUtil } from '../shared/interaction-util';
import { FilterType } from '../models/FilterType';

@Component({
  selector: 'app-journal-linechart',
  templateUrl: './journal-linechart.component.html',
  styleUrls: ['./journal-linechart.component.scss']
})
export class JournalLinechartComponent extends BaseGraphComponent implements OnInit {

  private paperDataHelperService: PaperDataHelperService;
  private currentData: any[] = []
  private rawData: any[] = [];

  private clickedJournal = ""
  private wasEverClickedBefore = false
  private wasDeselected = false

  private colorMap = ['#a6cee3', '#1f78b4', ' #b2df8a', '#33a02c',
    '#fb9a99', '#e31a1c', '#fdbf6f', '#ff7f00',
    '#cab2d6', '#6a3d9a', '#ffff99', '#b15928', '#c3c3c3']


  private parseYear = d3.timeParse("%Y");

  constructor(protected visdataService: VisDataService, paperDataHelperService: PaperDataHelperService) {
    super(visdataService);
    this.paperDataHelperService = paperDataHelperService;
  }

  override ngOnInit(): void {
    super.ngOnInit();
  }

  override prepareData(filteredResults: FilteredResults): any[] {
    let papers = filteredResults.getActivePapers()
    let dataMap = d3.rollup(papers, v => v.length, d => d.publicationYear);
    this.rawData = Array.from(dataMap).sort();

    let grouping = d3.group(papers, p => PaperDataHelperService.findJournalForTerm(p.publicationTitle));
    let groupingArray = Array.from(grouping);

    let groupingsMap: Map<string, any[]> = new Map();
    groupingArray.forEach((grouping) => {
      let rolledUpGrouping = d3.rollup(grouping[1], v => v.length, d => d.publicationYear)
      let groupingArray = Array.from(rolledUpGrouping).sort();
      groupingsMap.set(grouping[0], groupingArray);
    });

    let array = Array.from(groupingsMap);

    //TODO add missing years
 /*   let filteredYears = this.rawData.filter(function (d) { return d[0] != 0; });
    var years = filteredYears.map((d) => {return d[0]; })
    array.forEach(a => {
        var aYears =  a[1].map((d) => {return d[0]; })
        years.forEach(year=>{
          if(!aYears.includes(year)){
            a[1].push([year, 0])
          }
        })
    })*/
    return array;
  }

  override drawGraph(data: any[]): void {
    this.drawData(data)
  }

  override drawData(data: any[]): void {
    this.clearGraph()
    this.findToolTip();

    // Remember current data
    this.currentData = data

    let filteredYears = this.rawData.filter(function (d) { return d[0] != 0; });
    let minYear = this.parseYear(d3.min(filteredYears.map(d => d[0])));
    let maxYear = this.parseYear(d3.max(filteredYears.map(d => d[0])));

    let maxCount = this.findMax(data);

    // Create the x-axis scale
    if (minYear != null && maxYear != null) {
      const x = d3.scaleTime()
        .domain([minYear, maxYear])
        .range([0, this.width]);

      // Draw the x-axis
      this.svg.append("g")
        .attr("transform", "translate(0," + this.height + ")")
        .call(d3.axisBottom(x))
        .selectAll("text")
        .attr("transform", "translate(-10,0)rotate(-45)")
        .style("text-anchor", "end");

      // Create the y-axis scale
      const y = d3.scaleLog()
        .domain([1, maxCount])
        .range([this.height, 0]);

      // Draw the y-axis
      this.svg.append("g")
        .call(d3.axisLeft(y));

      // Now draw all the line charts
      data.forEach((grouping, index) => {
        let groupingData = grouping[1]
        let transformedData: PaperLineChartData[] = groupingData.map(item =>
          new PaperLineChartData(
            this.parseYear(item[0])!!,
            item[1],
            grouping[0]
          ))

        this.svg.append("path")
          .datum(transformedData)
          .attr("fill", "none")
          .attr("stroke", d => {
            let data = d
           // if (d[0].journal === this.clickedJournal || this.clickedJournal.length === 0 || !this.wasEverClickedBefore) {
              // Reset deselecting journal after drawing
              var id = PaperDataHelperService.journalMap.get(grouping[0])
              id = typeof id === "undefined" ? 0 : id;
              return this.colorMap[id]
          //  } else {
           //   return "#C0C0C077"
           // }

          })
          .attr("stroke-width", 2)
          .attr("d", d3.line()
            .x(function (d) {
              return x(d["year"])
            })
            .y(function (d) {
              return y(d["count"])
            })
          )
          .on("click", (line, d) => {
            this.selectJournal(line, d)
          })
          .on("mouseover", () => this.showToolTip())
          .on("mousemove", (event, line) => this.updateToolTip(event, line))
          .on("mouseout", () => this.hideToolTip());;
      });
    }

    this.drawLegend(data)
  }

  selectJournal(line: any, d: any) {
    this.wasEverClickedBefore = true

    if (d[0].journal == this.clickedJournal) {
      this.clickedJournal = ""
      this.wasDeselected = true

      // We have to remove the filter again
      this.visDataService.filter(new PaperFilter(
        -1,
        "",
        "",
        "",
        [],
        -1,
        -1,
        -1,
        [GraphsEnum.JOURNAL_LINECHART]), FilterType.Journal)

    } else {
      this.clickedJournal = d[0].journal

      // Otherwise regular filter operation
      this.visDataService.filter(new PaperFilter(
        -1,
        "",
        this.clickedJournal,
        "",
        [],
        -1,
        -1,
        -1,
        [GraphsEnum.JOURNAL_LINECHART]), FilterType.Journal)
    }

    // Redraw data

    this.drawData(this.currentData)
  }

  override getGraphConfig(): graphConfig.GraphConfig {
    return new graphConfig.GraphConfig(GraphsEnum.JOURNAL_LINECHART, "figure#journal_linechart", "Year", "Paper Count", true, true);
  }

  override getTitle(): string {
    return this.getGraphConfig().graphsEnum
  }

  private findMax(data: any[]): number {
    var max = 0;
    for (var d of data) {
      for (var chartData of d[1]) {
        if (chartData[1] > max) {
          max = chartData[1];
        }
      }
    }
    return max;
  }

  override clearGraph() {
    d3.selectAll("#journal_linechart" + " > svg > g > *").remove();

    if (this.legendSvg) {
      d3.selectAll("#journal_linechart" + " > svg > circle").remove();
      d3.selectAll("#journal_linechart" + " > svg > text").remove();
    }
  }

  override updateToolTip(event, line) {
    console.log("Move tool tip")
    this.toolTip.style("top", (event.pageY - 10) + "px").style("left", (event.pageX + 10) + "px").html(`Journal: <b>${line[0].journal}</b>`)
  }

  drawLegend(data: any) {
    if (this.legendSvg) {
      var stepX = this.width / 3
      var stepY = 20
      let columnSize = 3

      var iconX = this.width_margin
      var iconY = 20

      var textX = iconX + 20
      var textY = iconY

      data.forEach((grouping, index) => {
        var id = PaperDataHelperService.journalMap.get(grouping[0])
        id = typeof id === "undefined" ? 0 : id;
        this.legendSvg.append("circle")
          .attr("cx", iconX)
          .attr("cy", iconY)
          .attr("r", 6)
          .attr("stroke", "black")
          .style("fill", this.colorMap[id])

        this.legendSvg.append("text")
          .attr("x", textX)
          .attr("y", textY)
          .text(InteractionUtil.ellipsize(grouping[0])) // Accessing the text from the linechart grouping
          .style("font-size", "10px")
          .attr("alignment-baseline", "middle")

        if ((index + 1) % columnSize == 0) {
          iconX += stepX
          iconY = 20
        } else {
          iconY += stepY
        }

        // Always update text at the end
        textX = iconX + 20
        textY = iconY
      })
    }
  }
}

class PaperLineChartData {
  year: Date = new Date()
  count: number = 0
  journal: string = ""

  constructor(
    year: Date,
    count: number,
    journal: string) {
    this.journal = journal;
    this.year = year;
    this.count = count;
  }
}