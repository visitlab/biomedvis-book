import { EnrichedZoteroPaper } from "./EnrichedZoteroPaper"

export class AffiliationWrapper {
    enrichedZoteroPaper: EnrichedZoteroPaper
    affiliation: string = ""

    constructor(
        enrichedZoteroPaper: EnrichedZoteroPaper,
        affiliation: string
    ) {
        this.enrichedZoteroPaper = enrichedZoteroPaper
        this.affiliation = affiliation
    }
}