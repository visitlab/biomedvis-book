import { SimulationNodeDatum } from 'd3';

export class Author implements SimulationNodeDatum {
  id: number
  name: string
  outboundLinks: number
  isActive: boolean

  // Necessary for network graph implementation (d3 being weird..) - default to 0
  public x: number = 0;
  public y: number = 0;

  constructor(id: number, name: string, outboundLinks: number, isAuthorActive: boolean) {
    this.id = id;
    this.name = name;
    this.outboundLinks = outboundLinks;
    this.isActive = isAuthorActive;
  }
}

export class AuthorLink {
  firstAuthor: number
  secondAuthor: number
  linkingPaper: string
  isActive: boolean

  // Necessary for network graph implementation (d3 being weird..) - default to 0
  public source: number = 0;
  public target: number = 0;

  constructor(firstAuthor: number, secondAuthor: number, linkingPaper: string, isActive: boolean) {
    this.firstAuthor = firstAuthor;
    this.secondAuthor = secondAuthor;
    this.linkingPaper = linkingPaper;
    this.isActive = isActive;

    //Setting up d3 parts
    this.source = firstAuthor;
    this.target = secondAuthor;
  }
}
export class AuthorsAndLinks {
  authorSet: Set<Author>
  authorLinks: AuthorLink[]

  constructor(authorSet: Set<Author>, authorLinks: AuthorLink[]) {
    this.authorSet = authorSet;
    this.authorLinks = authorLinks;
  }
}