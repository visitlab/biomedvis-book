
export class EnrichedZoteroPaper {
    key: string = "" // position 0 
    itemType: string = "" // position 1
    publicationYear: string = "" // position 2
    author: string = "" // position 3
    title: string = "" // position 4
    publicationTitle: string = "" // position 5
    isbn: string = "" // position 6
    issn: string = "" // position 7
    doi: string = "" // position 8
    url: string = "" // position 9
    abstractNote: string = "" // position 10
    date: string = "" // position 11
    dateAdded: string = "" // position 12
    dateModified: string = "" // position 13
    accessDate: string = "" // position 14
    pages: string = "" // position 15
    issues: string = "" // position 17
    volume: string = "" // position 18
    journal: string = "" // position 40
    place: string = "" // position 27
    libraryCatalgoue: string = "" // position 33
    extra: string = "" // position 35
    automaticTags: string = "" // position 40
    conference: string = "" // position 40
    topic: string = "" // position 87
    area: string = "" // position 88
    affiliation: string = "" // position 89

    // Added for Filtering
    isActive: boolean = true
    
    constructor(
        key: string,
        itemType: string,
        publicationYear: string,
        author: string,
        title: string,
        publicationTitle: string,
        isbn: string,
        issn: string,
        doi: string,
        url: string,
        abstractNote: string,
        date: string,
        dateAdded: string,
        dateModified: string,
        pages: string,
        issues: string,
        volume: string,
        journal:string,
        place: string,
        libraryCatalogue: string,
        extra: string,
        automaticTags: string,
        conference:string,
        topic: string,
        area: string,
        affiliation: string) {
        this.key = key;
        this.itemType = itemType;
        this.publicationYear = publicationYear;
        this.author = author;
        this.title = title;
        this.publicationTitle = publicationTitle;
        this.isbn = isbn;
        this.issn = issn;
        this.doi = doi;
        this.url = url;
        this.abstractNote = abstractNote;
        this.date = date;
        this.dateAdded = dateAdded;
        this.dateModified = dateModified;
        this.pages = pages;
        this.issues = issues;
        this.volume = volume;
        this.place = place;
        this.libraryCatalgoue = libraryCatalogue;
        this.extra = extra;
        this.automaticTags = automaticTags;
        this.topic = topic;
        this.area = area,
        this.affiliation = affiliation;

        this.isActive = true;
    }
}