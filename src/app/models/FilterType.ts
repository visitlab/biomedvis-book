export enum FilterType {
    Subject,
    Topic,
    Journal,
    Affiliation,
    Author,
    Start,
    End,
    Count
  }