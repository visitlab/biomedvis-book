import { EnrichedZoteroPaper } from "./EnrichedZoteroPaper"
import { GraphsEnum } from "./GraphsEnum"

export class FilteredResults {

    private allPapers: EnrichedZoteroPaper[] = [];
    private lockedGraphs: GraphsEnum[] | null = [];
    private authorCount: number = -1

    constructor(
        allPapers: EnrichedZoteroPaper[] = [], 
        lockedGraphs: GraphsEnum[] = [],
        authorCount: number) {

            this.allPapers = allPapers
            this.lockedGraphs = lockedGraphs
            this.authorCount = authorCount
    }

    public getAllPapers(): EnrichedZoteroPaper[] {
        return this.allPapers
    }

    public getActivePapers(): EnrichedZoteroPaper[]{
        return this.allPapers.filter(
            function(p) {
                return p.isActive
            }
        )
    }

    public getInactivePapers(): EnrichedZoteroPaper[] {
        return this.allPapers.filter(
            function(p) {
                return !p.isActive
            }
        )
    }

    public getLockedGraphs(): GraphsEnum[] {
        if(this.lockedGraphs != null && this.lockedGraphs.length > 0) {
            return this.lockedGraphs
        } else {
            return []
        }
    }

    public getAuthorCount(): number {
       return this.authorCount
    }
}