export enum GraphsEnum {
    BLANK = "",
    JOURNAL_HISTOGRAM = "Journal Histogram",
    JOURNAL_LINECHART = "Journal Linechart",
    TOPIC_HISTOGRAM = "Topic Histogram",
    AFFILIATION_CHOLOPLETH = "Affiliation Choropleth",
    AUTHOR_LINK_HISTOGRAM = "Author Link Histogram",
    AUTHOR_NETWORK = "Author Network"
}