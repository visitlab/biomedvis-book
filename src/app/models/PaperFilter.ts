import { GraphsEnum } from "./GraphsEnum"

export class PaperFilter {

    private topic: number | null = -1 // chart-interactive 
    private subject: string | null = "" // subject is basically the same as title
    private journal: string | null = "" // chart-interactive 
    private affiliation: string | null = "" // chart-interactive 
    private authorNames: string[] | null = []; // chart-interactive 
    private startYear: number | null = -1 
    private endYear: number | null = -1
    private authorCount: number | null = -1

    private lockedGraphs: GraphsEnum[] | null = [];
    
    constructor(
        topic: number = -1,
        subject: string = "", 
        journal: string = "",
        affiliation: string = "",
        authorNames: string[] = [],
        startYear: number = -1,
        endYear: number = -1,
        authorCount: number = -1,
        lockedGraphs: GraphsEnum[] = []
    ) {
        // Setting up all given filters
        this.topic = topic
        this.subject = subject
        this.journal = journal
        this.affiliation = affiliation
        this.authorNames = authorNames
        this.startYear = startYear
        this.endYear = endYear
        this.authorCount = authorCount
        
        // Passing through locked graphs
        this.lockedGraphs = lockedGraphs
    }

    public getTopic(): number {
        if(this.topic != null) {
            return this.topic
        } else {
            return -1
        }
    }

    public setTopic(topic: number) {
       this.topic = topic
    }

    public getSubject(): string {
        if(this.subject != null) {
            return this.subject
        } else {
            return ""
        }
    }

    public setSubject(subject: string)  {
        this.subject = subject
    }

    public getJournal(): string {
        if(this.journal != null) {
            return this.journal
        } else {
            return ""
        }
    }

    public setJournal(journal: string) {
        this.journal = journal
    }

    public getAffiliation(): string {
        if(this.affiliation != null) {
            return this.affiliation
        } else {
            return ""
        }
    }

    public setAffiliation(affiliation: string) {
        this.affiliation = affiliation
    }

    public getAuthorNames(): string[] {
        if(this.authorNames === null || this.authorNames.length === 0) {
            return []
        } else {
            return this.authorNames
        }
    }

    public setAuthorNames(authorNames: string[]) {
        this.authorNames = authorNames
    }

    public getStartYear(): number {
        if(this.startYear != null) {
            return this.startYear
        } else {
            return -1
        }
    }

    public setStartYear(startYear: number) {
        this.startYear = startYear
    }

    public getEndYear(): number {
        if(this.endYear != null) {
            return this.endYear
        } else {
            return -1
        }
    }

    public setEndYear(endYear: number) {
        this.endYear = endYear
    }

    public getLockedGraphs(): GraphsEnum[] {
        if(this.lockedGraphs === null || this.lockedGraphs.length === 0) {
            return []
        } else {
            return this.lockedGraphs
        }
    } 

    public setLockedGraphs(lockedGraphs: GraphsEnum[]) {
        this.lockedGraphs = lockedGraphs
    } 

    public setAuthorCount(authorCount: number) {
        this.authorCount = authorCount
    }

    public getAuthorCount(): number {
        if(this.authorCount != null) {
            return this.authorCount
        } else {
            return -1
        }
    }
}