import { Component, OnInit } from '@angular/core';
import { EnrichedZoteroPaper } from '../models/EnrichedZoteroPaper';
import { VisDataService } from '../services/vis-data.service';
import { PageEvent } from '@angular/material/paginator'
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-paper-list',
  templateUrl: './paper-list.component.html',
  styleUrls: ['./paper-list.component.scss']
})
export class PaperListComponent implements OnInit {

  readonly PAGE_SIZE = 6

  cursorStart = 0
  cursorEnd = 0
  paperCount = 0

  protected visDataService: VisDataService;

  allPapers: EnrichedZoteroPaper[] = []
  currentPapers: EnrichedZoteroPaper[] = []

  constructor(visDataService: VisDataService) {
    this.visDataService = visDataService
  }

  ngOnInit(): void {
    this.visDataService.dataSubject.subscribe(data => {
      this.allPapers = data.getActivePapers()
      this.paperCount = this.allPapers.length
      this.cursorStart = 0
      this.cursorEnd = this.PAGE_SIZE

      this.currentPapers = this.sliceCurrentPapers()
    })
  }

  showNextPapers() {
    this.cursorStart += this.PAGE_SIZE
    this.cursorEnd += this.PAGE_SIZE
    this.currentPapers = this.sliceCurrentPapers()
  }

  showPreviousPapers() {
    this.cursorStart -= this.PAGE_SIZE
    this.cursorEnd -= this.PAGE_SIZE
    this.currentPapers = this.sliceCurrentPapers()
  }

  sliceCurrentPapers(): EnrichedZoteroPaper[] {
    return this.allPapers.slice(this.cursorStart, this.cursorEnd)
  }

  public getPaginatorData(event: PageEvent): PageEvent {
    this.cursorStart = event.pageIndex * event.pageSize
    this.cursorEnd = this.cursorStart + event.pageSize
    this.currentPapers = this.sliceCurrentPapers()
    return event;
  }

  onCloseListCLicked() {
    // Send hide command
    AppComponent.hiddenListSubject.next(true)
  }
}
