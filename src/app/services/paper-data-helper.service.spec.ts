import { TestBed } from '@angular/core/testing';

import { PaperDataHelperService } from './paper-data-helper.service';

describe('PaperDataHelperService', () => {
  let service: PaperDataHelperService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PaperDataHelperService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
