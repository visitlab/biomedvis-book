import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PaperDataHelperService {

  // This map maps the different topics from Topic 1 to Topic 10 to the corresponding terms
  // that have been found by the stop words algorithm 
  static topicMap = new Map<number, string[]>([
      [1, ["BioStructures"]],
      [2, ["Brain"]],
      [3, ["EHRs"]],
      [4, ["Education"]],
      [5, ["Gene Expression"]],
      [6, ["Networks"]],
      [7, ["Omics"]],
      [8, ["Other"]],
      [9, ["Public Health"]],
      [10, ["Surgery"]],
      [11, ["Techniques"]],
      [12, ["Treatment&Support"]],
      [13, ["Tumor"]],
      [14, ["Vascular"]]
   /*   [1, ["molecular", "simulation", "protein", "dynamic", "3d", "interactive", "surface", "interaction", "model", "time"]],
      [2, ["model", "3d", "human", "tissue", "anatomical", "brain", "image", "imaging", "body", "ultrasound"]],
      [3, ["volume", "rendering", "interactive", "function", "motion", "transfer", "volumetric", "quality", "image", "ray"]],
      [4, ["medical", "clinical", "image", "cancer", "treatment", "diagnosis", "decision", "uncertainty", "imaging", "learning"]],
      [5, ["virtual", "reality", "training", "augmented", "surgical", "technology", "surgery", "learning", "simulation", "immersive"]],
      [6, ["time", "map", "heart", "region", "cardiac", "tree", "clustering", "eeg", "space", "feature"]],
      [7, ["image", "segmentation", "surface", "3d", "cell", "ct", "feature", "shape", "detection", "automatic"]],
      [8, ["rehabilitation", "virtual", "reality", "game", "study", "stroke", "motor", "therapy", "child", "limb"]],
      [9, ["flow", "blood", "vessel", "stress", "mri", "cerebral", "aneurysm", "vascular", "wall", "artery"]],
      [10, ["study", "network", "domain", "interactive", "support", "complex", "time", "multiple", "expert", "well"]]*/
    ]);

    static journalMap = new Map<string, number>([
      ["EG VCBM", 0],
      ["IEEE TVCG", 1],      
      ["Computer Graphics Forum", 2],
      ["Bioinformatics", 3],
      ["IEEE VIS + VAST", 4],
      ["Computers & Graphics", 5],
      ["Nature", 6],  
      ["IEEE CG&A", 9],
      ["Other", 12]   

    ]);

    static journalCriterias = {
      "IEEE TVCG": function (term: string): boolean {
        return simpleMatch(term, "IEEE Transactions on Visualization and Computer Graphics");
      },
     // "IEEE VAHC": function (term: string): boolean {
      //  return simpleInclude(term, "IEEE Workshop on Visual Analytics in Healthcare");
      //},
      "IEEE VIS + VAST": function (term: string): boolean {
        return multipleInclude(term,
          ["IEEE Visualization",
            "IEEE VIS",
            "IEEE Conference on Visualization",
            "Proceedings Visualization",
            "IEEE Visualization",
            "Scientific Visualization Conference",
            "Symposium on Information Visualization",
            "Visual Analytics Science and Technology",
            "VAST",
          ]);
      },
      "Computer Graphics Forum": function (term: string): boolean {
        return simpleMatch(term, "Computer Graphics Forum");
      },
      //"IEEE PacificVis": function (term: string): boolean {
      //  return simpleInclude(term, "IEEE Pacific Visualization");
      //},
      "IEEE CG&A": function (term: string): boolean {
        return simpleMatch(term, "IEEE Computer Graphics and Applications");
      },
     // "IEEE Conference on Visual Analytics Science and Technology": function (term: string): boolean {
     //   return simpleInclude(term, "IEEE Conference on Visual Analytics Science and Technology");
     // },
      "Computers & Graphics": function (term: string): boolean {
        return simpleMatch(term, "Computers & Graphics")
      },
     // "Science": function (term: string): boolean {
     //   return multipleInclude(term, ["Science", "Scientific Reports", "Scientific Data"])
     // },
      "Nature": function (term: string): boolean {
        return simpleInclude(term, "Nature");
      },
      "EG VCBM": function (term: string): boolean {
        return simpleInclude(term, "Eurographics Workshop on Visual Computing for Biology and Medicine");
      },
      "Bioinformatics": function (term: string): boolean {
        return simpleMatch(term, "Bioinformatics");
      },
     // "IEEE Transactions on Biomedical Engineering": function (term: string): boolean {
      //  return simpleMatch(term, "IEEE Transactions on Biomedical Engineering");
     // },
     // "IEEE Transactions on Medical Imaging": function (term: string): boolean {
     //   return simpleMatch(term, "IEEE Transactions on Medical Imaging");
     // },
      //"IEEE": function (term: string): boolean {
      //  return simpleInclude(term, "IEEE");
      //},
      "Other": function (term: string): boolean {
        return true;
      }
    }

  constructor() { }


  // Resolving a topic term to a topic number
  findTopicForTerm(term: string): number {
    var output = -1
    PaperDataHelperService.topicMap.forEach((topics: string[], topicNumber: number) => {
      for (var topic of topics) {
        if((topic.includes(term) || term.includes(topic)) && term.length >= 3) {
          output = topicNumber;
        }
      }
    });
    return output;
  }

  public static findJournalForTerm(term: string): string{
    for (var s in PaperDataHelperService.journalCriterias) {
      if (PaperDataHelperService.journalCriterias[s](term) == true) {
        return s;
      }
    }
    return "Other";
  }

  static getTopicMap(): Map<number, string[]>{
    return this.topicMap;
  }
}

let simpleMatch: (term: string, searchTerm: string) => boolean = function (
  term: string,
  searchTerm: string
): boolean {
  return term === searchTerm;
}

let simpleInclude: (term: string, searchTerm: string) => boolean = function (
  term: string,
  searchTerm: string
): boolean {
  return term.includes(searchTerm);
}

let multipleInclude: (term: string, searchTerms: string[]) => boolean = function (
  term: string,
  searchTerms: string[]
): boolean {
  for (var s of searchTerms) {
    if (term.includes(s)) {
      return true;
    }
  }
  return false;
}
