import { TestBed } from '@angular/core/testing';

import { VisDataService } from './vis-data.service';

describe('VisDataService', () => {
  let service: VisDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VisDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
