import { Injectable } from '@angular/core';
import { Subject, EMPTY, Observable, of } from 'rxjs';
import { EnrichedZoteroPaper } from '../models/EnrichedZoteroPaper';
import { HttpClient } from '@angular/common/http';
import { NgxCsvParser, NgxCSVParserError } from 'ngx-csv-parser';
import { PaperFilter } from '../models/PaperFilter';
import { PaperDataHelperService } from '../services/paper-data-helper.service';
import { FilteredResults } from '../models/FilteredResults';
import { FilterType } from '../models/FilterType';
import * as interactionUtil from '../shared/interaction-util';

@Injectable({
  providedIn: 'root'
})
export class VisDataService {

  public currentFilter: PaperFilter = new PaperFilter()

  public dataSubject = new Subject<FilteredResults>();
  public paperFilterSubject = new Subject<PaperFilter>();

  readonly KEY_KEY = "Key";
  readonly KEY_ITEM_TYPE = "Item Type"
  readonly KEY_PUBLICATION_YEAR = "Publication Year"
  readonly KEY_AUTHOR = "Author"
  readonly KEY_TITLE = "Title"
  readonly KEY_PUBLICATION_TITLE = "Publication Title"
  readonly KEY_ISBN = "ISBN"
  readonly KEY_ISSN = "ISSN"
  readonly KEY_DOI = "DOI"
  readonly KEY_URL = "Url"
  readonly KEY_ABSTRACT = "Abstract Note"
  readonly KEY_DATE = "Date"
  readonly KEY_DATE_ADDED = "Date Added"
  readonly KEY_DATE_MODIFIED = "Date Modified"
  readonly KEY_ACCESS = "Access Date"
  readonly KEY_PAGES = "Pages"
  readonly KEY_ISSUES = "Issues"
  readonly KEY_VOLUME = "Volume"
  readonly KEY_JORNAL_ABR = "Journal Abbreviation"
  readonly KEY_PLACE = "Publisher"
  readonly KEY_LIBRARY = "Library Catalogue"
  readonly KEY_EXTRA = "Extra"
  readonly KEY_TAGS = "Automatic Tags"
  readonly KEY_CONFERENCE = "Conference Name"
  readonly KEY_TOPIC = "Topic"
  readonly KEY_AREA = "Area"
  readonly KEY_AFFILIATION = "Affiliation"

  private csvUrl = 'assets/withAff6.csv';
  private rawCsvData: any;

  private papers = new Array<EnrichedZoteroPaper>();

  constructor(private httpClient: HttpClient, private parser: NgxCsvParser) {
    this.httpClient.get(this.csvUrl, { responseType: 'blob' }).subscribe(
      data => {
        // Transform blob to csv file
        var csvFile: File = new File([data], "tempFile.csv")

        // Parse actual file now to push it to the papers collection
        parser.parse(csvFile, { header: true, delimiter: ',' }).pipe().subscribe({
          next: (result): void => {
            // Transform data to modeldata
            this.rawCsvData = result
            this.papers = this.rawCsvData.map(item => new EnrichedZoteroPaper(
              item[this.KEY_KEY],
              item[this.KEY_ITEM_TYPE],
              item[this.KEY_PUBLICATION_YEAR],
              item[this.KEY_AUTHOR],
              item[this.KEY_TITLE],
              item[this.KEY_PUBLICATION_TITLE],
              item[this.KEY_ISBN],
              item[this.KEY_ISSN],
              item[this.KEY_DOI],
              item[this.KEY_URL],
              item[this.KEY_ABSTRACT],
              item[this.KEY_DATE],
              item[this.KEY_DATE_ADDED],
              item[this.KEY_DATE_MODIFIED],
              item[this.KEY_PAGES],
              item[this.KEY_ISSUES],
              item[this.KEY_VOLUME],
              item[this.KEY_JORNAL_ABR],
              item[this.KEY_PLACE],
              item[this.KEY_LIBRARY],
              item[this.KEY_EXTRA],
              item[this.KEY_TAGS],
              item[this.KEY_CONFERENCE],
              item[this.KEY_TOPIC],
              item[this.KEY_AREA],
              item[this.KEY_AFFILIATION]
            ))

            // Remove the last element, should contain header descriptions
            this.papers.pop();

            // Update subject papers
            this.dataSubject.next(new FilteredResults(this.papers, [], this.currentFilter.getAuthorCount()))
          },
          error: (error: NgxCSVParserError): void => {
            console.log('Error while parsing paper csv', error);
          }
        });
      }
    )
  }

  public filter(paperFilter: PaperFilter, type: FilterType) {
    // Setup current filter
    this.adaptCurrentFilter(paperFilter, type)

    // Use current filter
    let filteredResults = this.getFilteredResults(this.currentFilter)
    this.dataSubject.next(filteredResults)
  }

  public resetFilter() {
    this.currentFilter = new PaperFilter();
  }

  public getFilteredResults(paperFilter: PaperFilter): FilteredResults {
    let papers: EnrichedZoteroPaper[] = [];

    for (var p of this.papers) {
      var isPaperActive = false

      if (this.isTopicFulFilled(p, paperFilter)
        && this.isSubjectFulfilled(p, paperFilter)
        && this.isJournalFulfilled(p, paperFilter)
        && this.isAffiliationFulFilled(p, paperFilter)
        && this.isAuthorFulfilled(p, paperFilter)
        && this.isPublicationYearFulfilled(p, paperFilter)) {
        isPaperActive = true
      }

      p.isActive = isPaperActive
      papers.push(p)
    }
    return new FilteredResults(papers, paperFilter.getLockedGraphs(), this.currentFilter.getAuthorCount())
  }

  private adaptCurrentFilter(paperFilter: PaperFilter, type: FilterType) {
    if (type == FilterType.Topic) {
      if (paperFilter.getTopic() + 1) {
        this.currentFilter.setTopic(paperFilter.getTopic())
      } else {
        this.currentFilter.setTopic(-1)
      }
    }

    if (type == FilterType.Subject) {
      if (paperFilter.getSubject() || paperFilter.getSubject() === "") {
        this.currentFilter.setSubject(paperFilter.getSubject())
      }
    }

    if (type == FilterType.Journal) {
      if (paperFilter.getJournal()) {
        this.currentFilter.setJournal(paperFilter.getJournal())
      } else {
        this.currentFilter.setJournal("")
      }
    }

    if (type == FilterType.Affiliation) {
      if (paperFilter.getAffiliation() || paperFilter.getAffiliation() === "") {
        this.currentFilter.setAffiliation(paperFilter.getAffiliation())
      }
    }

    if (type == FilterType.Author) {
      if (paperFilter.getAuthorNames()) {
        this.currentFilter.setAuthorNames(paperFilter.getAuthorNames())
      }
    }

    if (type == FilterType.End) {
      if (paperFilter.getEndYear()) {
        this.currentFilter.setEndYear(paperFilter.getEndYear())
      }
    }

    if (type == FilterType.Start) {
      if (paperFilter.getStartYear()) {
        this.currentFilter.setStartYear(paperFilter.getStartYear())
      }
    }

   // if (paperFilter.getLockedGraphs()) {
   //   this.currentFilter.setLockedGraphs(paperFilter.getLockedGraphs())
   // }

    if (type == FilterType.Count) {
      if (paperFilter.getAuthorCount()) {
        this.currentFilter.setAuthorCount(paperFilter.getAuthorCount())
      }
    }

    // Update paper filter subject
    this.paperFilterSubject.next(this.currentFilter)
  }

  public getCurrentFilter(){
    return this.currentFilter;
  }

  private isTopicFulFilled(paper: EnrichedZoteroPaper, paperFilter: PaperFilter): boolean {
    // Filter not active
    if (paperFilter.getTopic() == -1) {
      return true
    } else {
      try {
        var area = PaperDataHelperService.getTopicMap().get(paperFilter.getTopic() + 1) ?? ""
        if (paper.area == area[0]) {
          return true
        } else {
          return false
        }
      } catch (error) {
        console.error("Tried to process a paper with empty or wrong topic data")
        console.error(error)
        return false
      }
    }
  }

  private isSubjectFulfilled(paper: EnrichedZoteroPaper, paperFilter: PaperFilter): boolean {
    // Filter not active
    if (!paperFilter.getSubject() || paperFilter.getSubject() === "") {
      return true
    } else {
      if (paper.title.toLowerCase().includes(paperFilter.getSubject().toLowerCase()) && paperFilter.getSubject().trim().length >= 0) {
        return true
      } else {
        return false
      }
    }
  }

  private isJournalFulfilled(paper: EnrichedZoteroPaper, paperFilter: PaperFilter): boolean {
    // Filter not active
    if (!paperFilter.getJournal()) {
      return true
    } else {
      var title = PaperDataHelperService.findJournalForTerm(paper.publicationTitle)


      if ((paper.publicationTitle.includes(paperFilter.getJournal()) || title == paperFilter.getJournal()) && paperFilter.getJournal().trim().length > 0) {
        return true
      } else {
        return false
      }
    }
  }

  private isAffiliationFulFilled(paper: EnrichedZoteroPaper, paperFilter: PaperFilter): boolean {
    // Filter not active
    if (!paperFilter.getAffiliation()) {
      return true
    } else {
      if (paper.affiliation.includes(paperFilter.getAffiliation()) && paperFilter.getAffiliation().trim().length > 0) {
        return true
      } else {
        return false
      }
    }
  }

  private isAuthorFulfilled(paper: EnrichedZoteroPaper, paperFilter: PaperFilter): boolean {
    let isAuthorFulfilled = false
    if (!paperFilter.getAuthorNames() || paperFilter.getAuthorNames().length == 0) {
      return true
    } else {
      paperFilter.getAuthorNames().forEach((p) => {
        if (paper.author.includes(p)) {
          isAuthorFulfilled = true
        }
      });
      return isAuthorFulfilled
    }
  }

  private isPublicationYearFulfilled(paper: EnrichedZoteroPaper, paperFilter: PaperFilter): boolean {
    // Filter by year 
    let publicationYear = Number(paper.publicationYear)

    // Filter not active
    if (paperFilter.getStartYear() == -1 && paperFilter.getEndYear() == -1) {
      return true
    }

    if (paperFilter.getStartYear() > -1 && paperFilter.getEndYear() > -1) {
      if (publicationYear >= paperFilter.getStartYear() && publicationYear <= paperFilter.getEndYear()) {
        return true
      }
    } else if (paperFilter.getStartYear() > -1 && paperFilter.getEndYear() == -1) {
      if (publicationYear >= paperFilter.getStartYear()) {
        return true
      }
    } else if (paperFilter.getStartYear() == -1 && paperFilter.getEndYear() > -1) {
      if (publicationYear <= paperFilter.getEndYear()) {
        return true
      }
    }
    return false
  }
}
