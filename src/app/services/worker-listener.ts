import { AuthorsAndLinks } from "../models/AuthorsAndLinks";
import { EnrichedZoteroPaper } from "../models/EnrichedZoteroPaper";

export interface WorkerListener {
   onMessage(data: AuthorsAndLinks);
}