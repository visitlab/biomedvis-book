import { Injectable } from '@angular/core';
import { EnrichedZoteroPaper } from '../models/EnrichedZoteroPaper';
import { WorkerListener } from './worker-listener';
@Injectable({
  providedIn: 'root'
})
export class WorkerService {

  private worker;
  private workerListeners: WorkerListener[] = [];

  constructor() {
    this.terminateAndCreateWorker()
  }

  addWorkerListener(workerListener: WorkerListener) {
    this.workerListeners.push(workerListener)
  }

  postMessage(papers: EnrichedZoteroPaper[]) {
    this.terminateAndCreateWorker();
    this.worker.postMessage(papers);
  }

  private terminateAndCreateWorker() {
    if (typeof Worker !== 'undefined') {
      //Cancel current worker first if it was set...
      if (this.worker) {
        (this.worker as Worker).terminate()
      }
      // Create a new worker
      this.worker = new Worker(new URL('../app.worker', import.meta.url));

      this.worker.onmessage = ({ data }) => {
        this.workerListeners.forEach(function (it) {
          it.onMessage(data);
        }
        )
      }
    } else {
      // Web workers are not supported in this environment
      console.error("Web workers are not supported in this environment")
    }
  }
}
