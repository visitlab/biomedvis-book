import { area } from "d3";
import { GraphsEnum } from "../models/GraphsEnum";

export const CHART_HEIGHT_MARGIN = 48;
export const CHART_WIDTH_MARGIN = 48;
export const CHART_WIDTH = 680;
export const CHART_HEIGHT = 440;

export class GraphConfig {
    graphsEnum: GraphsEnum = GraphsEnum.BLANK
    internalName: string = "";
    xAxisLabel: string = "";
    yAxisLabel: string = "";
    areAxisLabelsNeeded: boolean = true;
    requestSpaceForLegend = false;

    constructor(graphsEnum: GraphsEnum, internalName: string, xAxisLabel: string, yAxisLabel: string, areAxisLabelsNeeded: boolean, requestSpaceForLegend: boolean) {
        this.graphsEnum = graphsEnum;
        this.internalName = internalName;
        this.xAxisLabel = xAxisLabel;
        this.yAxisLabel = yAxisLabel;
        this.areAxisLabelsNeeded = areAxisLabelsNeeded;
        this.requestSpaceForLegend = requestSpaceForLegend;
    } 
}