
export const KEY_ACTIVE = "active";
export const KEY_INACTIVE = "inactive";

export const ACTIVE_COLOR = "#a8c1b7";
export const INACTIVE_COLOR = "#d1d1d1";
export const INACTIVE_COLOR_ALPHA = "#d1d1d177";

export const CLICKED_COLOR = "#ff9d31";

export class InteractionUtil {

    static readonly LEGEND_STRING_LENGTH = 24

    constructor() {}
    
    public static ellipsize(input) {
        if (input.length > this.LEGEND_STRING_LENGTH) {
           return input.substring(0, this.LEGEND_STRING_LENGTH) + '...';
        }
        return input;
     };
}