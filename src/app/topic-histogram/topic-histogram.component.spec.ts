import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopicHistogramComponent } from './topic-histogram.component';

describe('TopicHistogramComponent', () => {
  let component: TopicHistogramComponent;
  let fixture: ComponentFixture<TopicHistogramComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [TopicHistogramComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopicHistogramComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
