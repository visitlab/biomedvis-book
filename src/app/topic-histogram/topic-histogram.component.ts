import { VisDataService } from '../services/vis-data.service';
import { BaseGraphComponent } from '../base-graph/base-graph.component';
import { EnrichedZoteroPaper } from '../models/EnrichedZoteroPaper';
import * as graphConfig from '../shared/graph-config';
import { Component, OnInit } from '@angular/core';
import * as d3 from 'd3';
import { PaperFilter } from '../models/PaperFilter';
import { GraphsEnum } from '../models/GraphsEnum';
import { FilteredResults } from '../models/FilteredResults';
import * as interactionUtil from '../shared/interaction-util';
import { PaperDataHelperService } from '../services/paper-data-helper.service';
import { FilterType } from '../models/FilterType';

@Component({
  selector: 'app-topic-histogram',
  templateUrl: './topic-histogram.component.html',
  styleUrls: ['./topic-histogram.component.scss']
})
export class TopicHistogramComponent extends BaseGraphComponent implements OnInit {

  private clickedTopic: number = -1
  private rawData: any[] = [];

  constructor(protected visdataService: VisDataService) {
    super(visdataService);
    this.height_margin = 68;
  }

  override ngOnInit(): void {
    super.ngOnInit();
  }

  override prepareData(filteredResults: FilteredResults): any[] {
    // Keep the raw rolled up data for graph axis drawing
    this.rawData = Array.from(d3.rollup(filteredResults.getAllPapers(), v => v.length, d => d.area)).sort();
    let papers = filteredResults.getAllPapers()

    let dataMap = d3.rollup(papers, v => v.length, d => d.area, d => d.isActive);
    let dataArray = Array.from(dataMap);
    let mappedForStacking = dataArray.map((value, _) => {

      // At this point values are stored in an InternMap with the key values of "true" and "false"
      // which reflect the state of the activeness of the filtered paper.
      // value[1] at that point is the isActive flag, as outcome of the rollup by publicationYear and isActive 
      let mappedJournalData: { [key: string]: number } = {

        // For now just drawing the number of the topic
        topic: this.rawData.map(d => d[0]).indexOf(value[0]),
        active: value[1].get(true)!,
        inactive: value[1].get(false)!
      }
      return mappedJournalData
    });

    let stackingOperation = d3.stack().keys([interactionUtil.KEY_ACTIVE, interactionUtil.KEY_INACTIVE]);
    let stackedMap = stackingOperation(mappedForStacking)
    return stackedMap
  }

  isTopicClicked(topic: number): number {
    if (this.clickedTopic == topic) {
      console.log("Topic clicked: " + topic)
      return 1
    } else {
      console.log("Topic not: " + topic)
      return 0
    }
  }

  override drawGraph(data: any[]): void {
    this.drawData(data)
  }

  override drawData(data: any[]): void {
    this.findToolTip();

    // Create the x-axis scale
    const x = d3.scaleBand()
      .range([0, this.width])
      .domain(this.rawData.map(d => d[0]))
      .padding(0.2);

    // Create a color scale
    var colorScale = d3.scaleOrdinal()
      .domain([interactionUtil.KEY_ACTIVE, interactionUtil.KEY_INACTIVE])
      .range([interactionUtil.ACTIVE_COLOR, interactionUtil.INACTIVE_COLOR]);

    // Draw the x-axis
    this.svg.append("g")
      .attr("transform", "translate(0," + (this.height) + ")")
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-40)")
      .style("text-anchor", "end");

    // Create the y-axis scale
    const y = d3.scaleLinear()
      .domain([0, Math.max.apply(Math, this.rawData.map(function (d) { return d[1] }))])
      .range([this.height, 0]);

    // Draw the y-axis
    this.svg.append("g")
      .call(d3.axisLeft(y));

    // Create and fill bars
    this.svg.selectAll("bars")
      .data(data)
      .enter().append("g")
      .attr("fill", d => {
        console.log("Current state of rolled up and stacked topics: " + d.key)
        return colorScale(d.key)
      })
      .selectAll("rect")
      .data(function (d) { return d; })
      .enter()
      .append("rect")
      // We introduce an id here and store it in the rect to handle the click listener reaction
      .attr("id", (d, i) => {
        return "topic" + d.data.topic
      })
      // Putting the topic x-Value back together correctly according to axis domain
      .attr("x", d => x(this.rawData.map(d => d[0])[d.data.topic]))
      .attr("y", d => y(d[1]))
      .attr("width", x.bandwidth())
      .attr("height", function (d) { return y(d[0]) - y(d[1]); })
      .on("mouseover", () => this.showToolTip())
      .on("mousemove", (event, bar) => this.updateToolTip(event, bar))
      .on("mouseout", () => this.hideToolTip())
      .on("click", (bar, d) => {
        // First find out what kind of action we have here
        var isSelect = true
        if(this.clickedTopic+1) {
          if(this.clickedTopic === bar.currentTarget.__data__.data["topic"]) {
            isSelect = false
          } else {
            isSelect = true
          }
        } else {
          isSelect = true
        }
        // Then react to the action accordingly
        if(isSelect) {
          // Clean up UI state old selection
          this.svg.select("#topic" + this.clickedTopic).style("fill", interactionUtil.ACTIVE_COLOR)

          // Update for new selection
          this.clickedTopic = bar.currentTarget.__data__.data["topic"]
          this.svg.select("#topic" + this.clickedTopic).style("fill", interactionUtil.CLICKED_COLOR)
          this.visDataService.filter(new PaperFilter(
            this.clickedTopic,
            "",
            "",
            "",
            [],
            -1,
            -1,
            -1,
            [GraphsEnum.TOPIC_HISTOGRAM]), FilterType.Topic)
        } else {
          // Otherwise we are handling a deselect action
          this.svg.select("#topic" + this.clickedTopic).style("fill", interactionUtil.ACTIVE_COLOR)
          this.clickedTopic = -1 

          this.visDataService.filter(new PaperFilter(
            -1,
            "",
            "",
            "",
            [],
            -1,
            -1,
            -1,
            [GraphsEnum.TOPIC_HISTOGRAM]),FilterType.Topic)
        }
      });
  }

  override getGraphConfig() {
    return new graphConfig.GraphConfig(GraphsEnum.TOPIC_HISTOGRAM, "figure#topics_histogram", "Topics", "Paper Count", true, false)
  }

  override getTitle(): string {
    return this.getGraphConfig().graphsEnum
  }

  override clearGraph() {
    d3.selectAll("#topics_histogram" + " > svg > g > *").remove();
  }

  override updateToolTip(event, bar) {
    console.log("Move tool tip")
    this.toolTip
      .style("top", (event.pageY - 10) + "px")
      .style("left", (event.pageX + 10) + "px")
      .html(`Topics: 
      <b>${PaperDataHelperService.getTopicMap().get(bar.data.topic+1)}</b>
      <br>
      Active papers: <b>${bar.data.active ?? 0}</b>
      <br>
      Inactive papers: <b>${bar.data.inactive ?? 0}</b>`)
  }
}
